module.exports = {
  plugins: ['@typescript-eslint'],
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  extends: [
    '@nuxtjs',
  ],
  rules: {
    semi: 'off',
    '@typescript-eslint/semi': ['error'],
    "quotes": ["error", "single"],
    "@typescript-eslint/no-unused-vars": 1,
    "space-before-function-paren": 1
  }
}
