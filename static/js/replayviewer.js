var Gokz;
(function (Gokz) {
    let SeekOrigin;
    (function (SeekOrigin) {
        SeekOrigin[SeekOrigin["Begin"] = 0] = "Begin";
        SeekOrigin[SeekOrigin["Current"] = 1] = "Current";
        SeekOrigin[SeekOrigin["End"] = 2] = "End";
    })(SeekOrigin = Gokz.SeekOrigin || (Gokz.SeekOrigin = {}));
    class BinaryReader {
        constructor(buffer) {
            this.buffer = buffer;
            this.view = new DataView(buffer);
            this.offset = 0;
        }
        seek(offset, origin) {
            switch (origin) {
                case SeekOrigin.Begin:
                    return (this.offset = offset);
                case SeekOrigin.End:
                    return (this.offset = this.buffer.byteLength - offset);
                default:
                    return (this.offset = this.offset + offset);
            }
        }
        getOffset() {
            return this.offset;
        }
        readUint8() {
            const value = this.view.getUint8(this.offset);
            this.offset += 1;
            return value;
        }
        readInt32() {
            const value = this.view.getInt32(this.offset, true);
            this.offset += 4;
            return value;
        }
        readUint32() {
            const value = this.view.getUint32(this.offset, true);
            this.offset += 4;
            return value;
        }
        readFloat32() {
            const value = this.view.getFloat32(this.offset, true);
            this.offset += 4;
            return value;
        }
        // http://www.onicos.com/staff/iz/amuse/javascript/expert/utf.txt
        /* utf.js - UTF-8 <=> UTF-16 convertion
         *
         * Copyright (C) 1999 Masanao Izumo <iz@onicos.co.jp>
         * Version: 1.0
         * LastModified: Dec 25 1999
         * This library is free.  You can redistribute it and/or modify it.
         */
        static utf8ArrayToStr(array) {
            var out, i, len, c;
            var char2, char3;
            out = '';
            len = array.length;
            i = 0;
            while (i < len) {
                c = array[i++];
                switch (c >> 4) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        // 0xxxxxxx
                        out += String.fromCharCode(c);
                        break;
                    case 12:
                    case 13:
                        // 110x xxxx   10xx xxxx
                        char2 = array[i++];
                        out += String.fromCharCode(((c & 0x1f) << 6) | (char2 & 0x3f));
                        break;
                    case 14:
                        // 1110 xxxx  10xx xxxx  10xx xxxx
                        char2 = array[i++];
                        char3 = array[i++];
                        out += String.fromCharCode(((c & 0x0f) << 12) | ((char2 & 0x3f) << 6) | ((char3 & 0x3f) << 0));
                        break;
                }
            }
            return out;
        }
        readString(length) {
            if (length === undefined) {
                length = this.readUint8();
            }
            let chars = new Array(length);
            for (let i = 0; i < length; ++i) {
                chars[i] = this.readUint8();
            }
            return BinaryReader.utf8ArrayToStr(chars);
        }
        readVector2(vec) {
            if (vec === undefined)
                vec = new Facepunch.Vector2();
            vec.set(this.readFloat32(), this.readFloat32());
            return vec;
        }
        readVector3(vec) {
            if (vec === undefined)
                vec = new Facepunch.Vector3();
            vec.set(this.readFloat32(), this.readFloat32(), this.readFloat32());
            return vec;
        }
    }
    Gokz.BinaryReader = BinaryReader;
})(Gokz || (Gokz = {}));
var Gokz;
(function (Gokz) {
    class Event {
        constructor(sender) {
            this.handlers = [];
            this.sender = sender;
        }
        addListener(handler) {
            this.handlers.push(handler);
        }
        removeListener(handler) {
            const index = this.handlers.indexOf(handler);
            if (index === -1)
                return false;
            this.handlers.splice(index, 1);
            return true;
        }
        clearListeners() {
            this.handlers = [];
        }
        dispatch(args) {
            const count = this.handlers.length;
            for (let i = 0; i < count; ++i) {
                this.handlers[i](args, this.sender);
            }
        }
    }
    Gokz.Event = Event;
    class ChangedEvent extends Event {
        constructor(sender, equalityComparison) {
            super(sender);
            if (equalityComparison != null) {
                this.equalityComparison = equalityComparison;
            }
            else {
                this.equalityComparison = (a, b) => a === b;
            }
        }
        reset() {
            this.prevValue = undefined;
        }
        update(value, args) {
            if (this.equalityComparison(this.prevValue, value))
                return;
            this.prevValue = value;
            this.dispatch(args === undefined ? value : args);
        }
    }
    Gokz.ChangedEvent = ChangedEvent;
})(Gokz || (Gokz = {}));
var Gokz;
(function (Gokz) {
    let GlobalMode;
    (function (GlobalMode) {
        GlobalMode[GlobalMode["Vanilla"] = 0] = "Vanilla";
        GlobalMode[GlobalMode["KzSimple"] = 1] = "KzSimple";
        GlobalMode[GlobalMode["KzTimer"] = 2] = "KzTimer";
    })(GlobalMode = Gokz.GlobalMode || (Gokz.GlobalMode = {}));
    let GlobalStyle;
    (function (GlobalStyle) {
        GlobalStyle[GlobalStyle["Normal"] = 0] = "Normal";
    })(GlobalStyle = Gokz.GlobalStyle || (Gokz.GlobalStyle = {}));
    let Button;
    (function (Button) {
        Button[Button["Attack"] = 1] = "Attack";
        Button[Button["Jump"] = 2] = "Jump";
        Button[Button["Duck"] = 4] = "Duck";
        Button[Button["Forward"] = 8] = "Forward";
        Button[Button["Back"] = 16] = "Back";
        Button[Button["Use"] = 32] = "Use";
        Button[Button["Cancel"] = 64] = "Cancel";
        Button[Button["Left"] = 128] = "Left";
        Button[Button["Right"] = 256] = "Right";
        Button[Button["MoveLeft"] = 512] = "MoveLeft";
        Button[Button["MoveRight"] = 1024] = "MoveRight";
        Button[Button["Attack2"] = 2048] = "Attack2";
        Button[Button["Run"] = 4096] = "Run";
        Button[Button["Reload"] = 8192] = "Reload";
        Button[Button["Alt1"] = 16384] = "Alt1";
        Button[Button["Alt2"] = 32768] = "Alt2";
        Button[Button["Score"] = 65536] = "Score";
        Button[Button["Speed"] = 131072] = "Speed";
        Button[Button["Walk"] = 262144] = "Walk";
        Button[Button["Zoom"] = 524288] = "Zoom";
        Button[Button["Weapon1"] = 1048576] = "Weapon1";
        Button[Button["Weapon2"] = 2097152] = "Weapon2";
        Button[Button["BullRush"] = 4194304] = "BullRush";
        Button[Button["Grenade1"] = 8388608] = "Grenade1";
        Button[Button["Grenade2"] = 16777216] = "Grenade2";
    })(Button = Gokz.Button || (Gokz.Button = {}));
    let EntityFlag;
    (function (EntityFlag) {
        EntityFlag[EntityFlag["OnGround"] = 1] = "OnGround";
        EntityFlag[EntityFlag["Ducking"] = 2] = "Ducking";
        EntityFlag[EntityFlag["WaterJump"] = 4] = "WaterJump";
        EntityFlag[EntityFlag["OnTrain"] = 8] = "OnTrain";
        EntityFlag[EntityFlag["InRain"] = 16] = "InRain";
        EntityFlag[EntityFlag["Frozen"] = 32] = "Frozen";
        EntityFlag[EntityFlag["AtControls"] = 64] = "AtControls";
        EntityFlag[EntityFlag["Client"] = 128] = "Client";
        EntityFlag[EntityFlag["FakeClient"] = 256] = "FakeClient";
        EntityFlag[EntityFlag["InWater"] = 512] = "InWater";
        EntityFlag[EntityFlag["Fly"] = 1024] = "Fly";
        EntityFlag[EntityFlag["Swim"] = 2048] = "Swim";
        EntityFlag[EntityFlag["Conveyor"] = 4096] = "Conveyor";
        EntityFlag[EntityFlag["Npc"] = 8192] = "Npc";
        EntityFlag[EntityFlag["GodMode"] = 16384] = "GodMode";
        EntityFlag[EntityFlag["NoTarget"] = 32768] = "NoTarget";
        EntityFlag[EntityFlag["AimTarget"] = 65536] = "AimTarget";
        EntityFlag[EntityFlag["PartialGround"] = 131072] = "PartialGround";
        EntityFlag[EntityFlag["StaticProp"] = 262144] = "StaticProp";
        EntityFlag[EntityFlag["Graphed"] = 524288] = "Graphed";
        EntityFlag[EntityFlag["Grenade"] = 1048576] = "Grenade";
        EntityFlag[EntityFlag["StepMovement"] = 2097152] = "StepMovement";
        EntityFlag[EntityFlag["DontTouch"] = 4194304] = "DontTouch";
        EntityFlag[EntityFlag["BaseVelocity"] = 8388608] = "BaseVelocity";
        EntityFlag[EntityFlag["WorldBrush"] = 16777216] = "WorldBrush";
        EntityFlag[EntityFlag["Object"] = 33554432] = "Object";
        EntityFlag[EntityFlag["KillMe"] = 67108864] = "KillMe";
        EntityFlag[EntityFlag["OnFire"] = 134217728] = "OnFire";
        EntityFlag[EntityFlag["Dissolving"] = 268435456] = "Dissolving";
        EntityFlag[EntityFlag["TransRagdoll"] = 536870912] = "TransRagdoll";
        EntityFlag[EntityFlag["UnblockableByPlayer"] = 1073741824] = "UnblockableByPlayer";
        EntityFlag[EntityFlag["Freezing"] = -2147483648] = "Freezing";
    })(EntityFlag = Gokz.EntityFlag || (Gokz.EntityFlag = {}));
    class TickData {
        constructor() {
            this.position = new Facepunch.Vector3();
            this.angles = new Facepunch.Vector2();
            this.tick = -1;
            this.buttons = 0;
            this.flags = 0;
        }
        getEyeHeight() {
            return (this.flags & EntityFlag.Ducking) != 0 ? 46 : 64;
        }
    }
    Gokz.TickData = TickData;
    class ReplayFile {
        constructor(data) {
            const reader = (this.reader = new Gokz.BinaryReader(data));
            const magic = reader.readInt32();
            if (magic !== ReplayFile.MAGIC) {
                throw 'Unrecognised replay file format.';
            }
            this.formatVersion = reader.readUint8();
            this.pluginVersion = reader.readString();
            this.mapName = reader.readString();
            this.course = reader.readInt32();
            this.mode = reader.readInt32();
            this.style = reader.readInt32();
            this.time = reader.readFloat32();
            this.teleportsUsed = reader.readInt32();
            this.steamId = reader.readInt32();
            this.steamId2 = reader.readString();
            reader.readString();
            this.playerName = reader.readString();
            this.tickCount = reader.readInt32();
            this.tickRate = Math.round(this.tickCount / this.time); // todo
            this.firstTickOffset = reader.getOffset();
            this.tickSize = 7 * 4;
        }
        getTickData(tick, data) {
            if (data === undefined)
                data = new TickData();
            data.tick = tick;
            const reader = this.reader;
            reader.seek(this.firstTickOffset + this.tickSize * tick, Gokz.SeekOrigin.Begin);
            reader.readVector3(data.position);
            reader.readVector2(data.angles);
            data.buttons = reader.readInt32();
            data.flags = reader.readInt32();
            return data;
        }
        clampTick(tick) {
            return tick < 0 ? 0 : tick >= this.tickCount ? this.tickCount - 1 : tick;
        }
    }
    ReplayFile.MAGIC = 0x676f6b7a;
    Gokz.ReplayFile = ReplayFile;
})(Gokz || (Gokz = {}));
///<reference path="../js/facepunch.webgame.d.ts"/>
///<reference path="../js/sourceutils.d.ts"/>
var WebGame = Facepunch.WebGame;
var Gokz;
(function (Gokz) {
    /**
     * Creates a GOKZ replay viewer applet.
     */
    class ReplayViewer extends SourceUtils.MapViewer {
        /**
         * Creates a new ReplayViewer inside the given `container` element.
         * @param container Element that should contain the viewer.
         */
        constructor(container) {
            super(container);
            /**
             * If true, the current tick will be stored in the address hash when
             * playback is paused or the viewer uses the playback bar to skip
             * around.
             * @default `true`
             */
            this.saveTickInHash = true;
            /**
             * The current tick being shown during playback, starting with 0 for
             * the first tick. Will automatically be increased while playing,
             * although some ticks might be skipped depending on playback speed and
             * frame rate. Can be set to skip to a particular tick.
             */
            this.tick = -1;
            /**
             * Current playback rate, measured in seconds per second. Can support
             * negative values for rewinding.
             * @default `1.0`
             */
            this.playbackRate = 1.0;
            /**
             * If true, the replay will automatically loop back to the first tick
             * when it reaches the end.
             * @default `true`
             */
            this.autoRepeat = true;
            /**
             * Used internally to temporarily pause playback while the user is
             * dragging the scrubber in the playback bar.
             */
            this.isScrubbing = false;
            /**
             * If true, the currently displayed tick will advance based on the
             * value of `playbackRate`.
             * @default `false`
             */
            this.isPlaying = false;
            /**
             * Event invoked when a new replay is loaded. Will be invoked before
             * the map for the replay is loaded (if required).
             *
             * **Available event arguments**:
             * * `replay: Gokz.ReplayFile` - The newly loaded ReplayFile
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.replayLoaded = new Gokz.Event(this);
            /**
             * Event invoked after each update.
             *
             * **Available event arguments**:
             * * `dt: number` - Time since the last update
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.updated = new Gokz.Event(this);
            this.messageEvent = new Gokz.Event(this);
            /**
             * Event invoked when the current tick has changed.
             *
             * **Available event arguments**:
             * * `tickData: Gokz.TickData` - Recorded data for the current tick
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.tickChanged = new Gokz.ChangedEvent(this);
            /**
             * Event invoked when playback has skipped to a different tick, for
             * example when the user uses the scrubber.
             *
             * **Available event arguments**:
             * * `oldTick: number` - The previous value of `tick` before skipping
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.playbackSkipped = new Gokz.Event(this);
            /**
             * Event invoked when `playbackRate` changes.
             *
             * **Available event arguments**:
             * * `playbackRate: number` - The new playback rate
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.playbackRateChanged = new Gokz.ChangedEvent(this);
            /**
             * Event invoked when `isPlaying` changes, for example when the user
             * pauses or resumes playback.
             *
             * **Available event arguments**:
             * * `isPlaying: boolean` - True if currently playing
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.isPlayingChanged = new Gokz.ChangedEvent(this);
            /**
             * Event invoked when `cameraMode` changes.
             *
             * **Available event arguments**:
             * * `cameraMode: SourceUtils.CameraMode` - Camera mode value
             * * `sender: Gokz.ReplayViewer` - This ReplayViewer
             */
            this.cameraModeChanged = new Gokz.ChangedEvent(this);
            this.pauseTime = 1.0;
            this.spareTime = 0;
            this.prevTick = undefined;
            this.tickData = new Gokz.TickData();
            this.tempTickData0 = new Gokz.TickData();
            this.tempTickData1 = new Gokz.TickData();
            this.tempTickData2 = new Gokz.TickData();
            this.saveCameraPosInHash = false;
            this.isPlayingChanged.addListener(isPlaying => {
                if (!isPlaying && this.saveTickInHash)
                    this.updateTickHash();
                if (isPlaying) {
                    this.wakeLock = navigator.wakeLock;
                    if (this.wakeLock != null) {
                        this.wakeLock.request('display');
                    }
                    this.cameraMode = SourceUtils.CameraMode.Fixed;
                }
                else if (this.wakeLock != null) {
                    this.wakeLock.release('display');
                    this.wakeLock = null;
                }
            });
            this.cameraModeChanged.addListener(mode => {
                if (mode === SourceUtils.CameraMode.FreeCam) {
                    this.isPlaying = false;
                }
                if (this.routeLine != null) {
                    this.routeLine.visible = mode === SourceUtils.CameraMode.FreeCam;
                }
                this.canLockPointer = mode === SourceUtils.CameraMode.FreeCam;
                if (!this.canLockPointer && this.isPointerLocked()) {
                    document.exitPointerLock();
                }
            });
        }
        showMessage(message) {
            this.messageEvent.dispatch(message);
        }
        /**
         * Attempt to load a GOKZ replay from the given URL. When loaded, the
         * replay will be stored in the `replay` property in this viewer.
         * @param url Url of the replay to download.
         */
        loadReplay(url) {
            console.log(`Downloading: ${url}`);
            const req = new XMLHttpRequest();
            req.open('GET', url, true);
            req.responseType = 'arraybuffer';
            req.onload = ev => {
                if (req.status !== 200) {
                    this.showMessage(`Unable to download replay: ${req.statusText}`);
                    return;
                }
                const arrayBuffer = req.response;
                if (arrayBuffer) {
                    if (this.routeLine != null) {
                        this.routeLine.dispose();
                        this.routeLine = null;
                    }
                    try {
                        this.replay = new Gokz.ReplayFile(arrayBuffer);
                    }
                    catch (e) {
                        this.showMessage(`Unable to read replay: ${e}`);
                    }
                }
            };
            req.send(null);
        }
        /**
         * If `saveTickInHash` is true, will set the address hash to include
         * the current tick number.
         */
        updateTickHash() {
            if (this.replay == null || !this.saveTickInHash)
                return;
            this.setHash({ t: this.replay.clampTick(this.tick) + 1 });
        }
        onInitialize() {
            super.onInitialize();
            this.canLockPointer = false;
            this.cameraMode = SourceUtils.CameraMode.Fixed;
        }
        onHashChange(hash) {
            if (typeof hash === 'string')
                return;
            if (!this.saveTickInHash)
                return;
            const data = hash;
            if (data.t !== undefined && this.tick !== data.t) {
                this.tick = data.t - 1;
                this.isPlaying = false;
            }
        }
        onChangeReplay(replay) {
            this.pauseTicks = Math.round(replay.tickRate * this.pauseTime);
            this.tick = this.tick === -1 ? 0 : this.tick;
            this.spareTime = 0;
            this.prevTick = undefined;
            this.replayLoaded.dispatch(this.replay);
            if (this.currentMapName !== replay.mapName) {
                if (this.currentMapName != null) {
                    this.map.unload();
                }
                if (this.mapBaseUrl == null) {
                    throw 'Cannot load a map when mapBaseUrl is unspecified.';
                }
                const version = new Date().getTime().toString(16);
                this.currentMapName = replay.mapName;
                this.loadMap(`${this.mapBaseUrl}/${replay.mapName}/index.json?v=${version}`);
            }
        }
        onUpdateFrame(dt) {
            super.onUpdateFrame(dt);
            if (this.replay != this.lastReplay) {
                this.lastReplay = this.replay;
                if (this.replay != null) {
                    this.onChangeReplay(this.replay);
                }
            }
            this.playbackRateChanged.update(this.playbackRate);
            this.cameraModeChanged.update(this.cameraMode);
            if (this.replay == null) {
                this.updated.dispatch(dt);
                return;
            }
            const replay = this.replay;
            const tickPeriod = 1.0 / replay.tickRate;
            this.isPlayingChanged.update(this.isPlaying);
            if (this.prevTick !== undefined && this.tick !== this.prevTick) {
                this.playbackSkipped.dispatch(this.prevTick);
            }
            if (this.routeLine == null && this.map.isReady()) {
                this.routeLine = new Gokz.RouteLine(this.map, this.replay);
            }
            if (this.map.isReady() && this.isPlaying && !this.isScrubbing) {
                this.spareTime += dt * this.playbackRate;
                const oldTick = this.tick;
                // Forward playback
                while (this.spareTime > tickPeriod) {
                    this.spareTime -= tickPeriod;
                    this.tick += 1;
                    if (this.tick > replay.tickCount + this.pauseTicks * 2) {
                        this.tick = -this.pauseTicks;
                    }
                }
                // Rewinding
                while (this.spareTime < 0) {
                    this.spareTime += tickPeriod;
                    this.tick -= 1;
                    if (this.tick < -this.pauseTicks * 2) {
                        this.tick = replay.tickCount + this.pauseTicks;
                    }
                }
            }
            else {
                this.spareTime = 0;
            }
            this.prevTick = this.tick;
            replay.getTickData(replay.clampTick(this.tick), this.tickData);
            let eyeHeight = this.tickData.getEyeHeight();
            this.tickChanged.update(this.tick, this.tickData);
            if (this.spareTime >= 0 && this.spareTime <= tickPeriod) {
                const t = this.spareTime / tickPeriod;
                const d0 = replay.getTickData(replay.clampTick(this.tick - 1), this.tempTickData0);
                const d1 = this.tickData;
                const d2 = replay.getTickData(replay.clampTick(this.tick + 1), this.tempTickData1);
                const d3 = replay.getTickData(replay.clampTick(this.tick + 2), this.tempTickData2);
                Gokz.Utils.hermitePosition(d0.position, d1.position, d2.position, d3.position, t, this.tickData.position);
                Gokz.Utils.hermiteAngles(d0.angles, d1.angles, d2.angles, d3.angles, t, this.tickData.angles);
                eyeHeight = Gokz.Utils.hermiteValue(d0.getEyeHeight(), d1.getEyeHeight(), d2.getEyeHeight(), d3.getEyeHeight(), t);
            }
            if (this.cameraMode === SourceUtils.CameraMode.Fixed) {
                this.mainCamera.setPosition(this.tickData.position.x, this.tickData.position.y, this.tickData.position.z + eyeHeight);
                this.setCameraAngles(((this.tickData.angles.y - 90) * Math.PI) / 180, (-this.tickData.angles.x * Math.PI) / 180);
            }
            this.updated.dispatch(dt);
        }
        toggleRoute() {
            this.routeLine.visible = !this.routeLine.visible;
        }
    }
    Gokz.ReplayViewer = ReplayViewer;
})(Gokz || (Gokz = {}));
var Gokz;
(function (Gokz) {
    class RouteLine extends SourceUtils.Entities.PvsEntity {
        constructor(map, replay) {
            super(map, { classname: 'route_line', clusters: null });
            this.isVisible = false;
            this.segments = new Array(Math.ceil(replay.tickCount / RouteLine.segmentTicks));
            const tickData = new Gokz.TickData();
            const progressScale = 16 / replay.tickRate;
            const lastPos = new Facepunch.Vector3();
            const currPos = new Facepunch.Vector3();
            for (let i = 0; i < this.segments.length; ++i) {
                const firstTick = i * RouteLine.segmentTicks;
                const lastTick = Math.min((i + 1) * RouteLine.segmentTicks, replay.tickCount - 1);
                const segment = (this.segments[i] = {
                    debugLine: new WebGame.DebugLine(map.viewer),
                    clusters: {}
                });
                const debugLine = segment.debugLine;
                const clusters = segment.clusters;
                debugLine.setColor({ x: 0.125, y: 0.75, z: 0.125 }, { x: 0.0, y: 0.25, z: 0.0 });
                debugLine.frequency = 4.0;
                let lineStartTick = firstTick;
                for (let t = firstTick; t <= lastTick; ++t) {
                    replay.getTickData(t, tickData);
                    currPos.copy(tickData.position);
                    currPos.z += 16;
                    const leaf = map.getLeafAt(currPos);
                    if (leaf != null && leaf.cluster !== -1) {
                        clusters[leaf.cluster] = true;
                    }
                    // Start new line if first in segment or player teleported
                    if (t === firstTick || lastPos.sub(currPos).lengthSq() > 1024.0) {
                        debugLine.moveTo(currPos);
                        lineStartTick = t;
                    }
                    else {
                        debugLine.lineTo(currPos, (t - lineStartTick) * progressScale);
                    }
                    lastPos.copy(currPos);
                }
                debugLine.update();
            }
        }
        get visible() {
            return this.isVisible;
        }
        set visible(value) {
            if (this.isVisible === value)
                return;
            this.isVisible = value;
            if (value) {
                this.map.addPvsEntity(this);
            }
            else {
                this.map.removePvsEntity(this);
            }
            this.map.viewer.forceDrawListInvalidation(true);
        }
        onPopulateDrawList(drawList, clusters) {
            for (let segment of this.segments) {
                if (clusters == null) {
                    drawList.addItem(segment.debugLine);
                    continue;
                }
                const segmentClusters = segment.clusters;
                for (let cluster of clusters) {
                    if (segmentClusters[cluster]) {
                        drawList.addItem(segment.debugLine);
                        break;
                    }
                }
            }
        }
        dispose() {
            this.visible = false;
            for (let segment of this.segments) {
                segment.debugLine.dispose();
            }
            this.segments.splice(0, this.segments.length);
        }
    }
    RouteLine.segmentTicks = 60 * 128;
    Gokz.RouteLine = RouteLine;
})(Gokz || (Gokz = {}));
var Gokz;
(function (Gokz) {
    class Utils {
        static deltaAngle(a, b) {
            return b - a - Math.floor((b - a + 180) / 360) * 360;
        }
        static hermiteValue(p0, p1, p2, p3, t) {
            const m0 = (p2 - p0) * 0.5;
            const m1 = (p3 - p1) * 0.5;
            const t2 = t * t;
            const t3 = t * t * t;
            return ((2 * t3 - 3 * t2 + 1) * p1 +
                (t3 - 2 * t2 + t) * m0 +
                (-2 * t3 + 3 * t2) * p2 +
                (t3 - t2) * m1);
        }
        static hermitePosition(p0, p1, p2, p3, t, out) {
            out.x = Utils.hermiteValue(p0.x, p1.x, p2.x, p3.x, t);
            out.y = Utils.hermiteValue(p0.y, p1.y, p2.y, p3.y, t);
            out.z = Utils.hermiteValue(p0.z, p1.z, p2.z, p3.z, t);
        }
        static hermiteAngles(a0, a1, a2, a3, t, out) {
            out.x = Utils.hermiteValue(a1.x + Utils.deltaAngle(a1.x, a0.x), a1.x, a1.x + Utils.deltaAngle(a1.x, a2.x), a1.x + Utils.deltaAngle(a1.x, a3.x), t);
            out.y = Utils.hermiteValue(a1.y + Utils.deltaAngle(a1.y, a0.y), a1.y, a1.y + Utils.deltaAngle(a1.y, a2.y), a1.y + Utils.deltaAngle(a1.y, a3.y), t);
        }
    }
    Gokz.Utils = Utils;
})(Gokz || (Gokz = {}));
